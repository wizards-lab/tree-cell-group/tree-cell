export {
  scheduleRootCellActualization,
  actualizeScheduledCells,
  isActualizationProcessGoingOnNow,
  isActualizationProcessAlreadyScheduled,
  isCellScheduled,
  scheduleDeactivation,
  CyclicActualizeOfScheduledCellsError,
} from './riims.sheduler';
